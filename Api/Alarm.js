const express = require('express')
const mongoose = require('mongoose')
const Alarm = require('../DB/Alarm')
const route = express.Router();

// create new alarm with post request
route.post('/newAlarm', async(req,res)=>{
    const{hours,minutes,active} = req.body;
    let alarm = {}
    alarm.hours = hours;
    alarm.minutes = minutes;
    alarm.active = active;
    let alarmModel = new Alarm(alarm);
    await alarmModel.save();
    res.json(alarmModel);
})

// get all alarms
route.get('/allAlarms', async(req,res)=>{
    Alarm.find(function (err, alrm) {
            if (err) return console.error(err);
            res.json(alrm);
        })
})

// set off alarm
route.put('/setOff', async(req, res) =>{
    const{id} = req.body;
    const query = { "_id": id };
    const update = {"active":false,"update":Date.now()};
    Alarm.updateOne(query,  
      update, function (err, docs) { 
      if (err){ 
          console.log(err) 
          res.send(err);
      } 
      else{ 
          console.log("Updated Docs : ", docs); 
          res.send("Updated Docs : " + docs);
      } 
  });
    })

    // delete
    route.delete('/delete', async(req, res) =>{
        const{id} = req.body;
        const query = { "_id": id };
        const update = {"active":false,"update":Date.now()};
        Alarm.deleteOne(query, function (err, docs) { 
          if (err){ 
              console.log(err) 
              res.send(err);
          } 
          else{ 
              console.log("Deleted Docs : ", docs); 
              res.send("Deleted Docs : " + docs);
          } 
      });
        })

// update off alarm
route.put('/update', async(req, res) =>{
    const {id,hours,minutes,active} = req.body;
    let alarm = {}
    alarm.hours = hours;
    alarm.minutes = minutes;
    alarm.active = active;
    const query = { "_id": id };
    const update = {"hours":hours,"minutes":minutes,"active":active,"update":Date.now()};
    console.log(update)
    Alarm.updateOne(query,  
      update, function (err, docs) { 
      if (err){ 
          console.log(err) 
          res.send(err);
      } 
      else{ 
          console.log("Updated Docs : ", docs); 
          res.send("Updated Docs : " + docs);
      } 
  });
    })

module.exports = route;
