const mongoose = require('mongoose')

const alarm = new mongoose.Schema({
    hours:{
        type:Number
    },
    minutes:{
        type:Number
    },
    active:{
        type:Boolean
    },
    update:{
        type:Date, default:Date.now
    }
})

module.exports = Alarm = mongoose.model('alarm',alarm)